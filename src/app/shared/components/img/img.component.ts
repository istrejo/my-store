import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  AfterViewInit,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss'],
})
export class ImgComponent {
  img: string = '';

  @Input('img')
  set changeImg(newImg: string) {
    this.img = newImg;
    // console.log('change just img =>', this.img);
    //code
  }
  @Input() alt: string = '';
  @Output() loaded = new EventEmitter<string>();
  imageDefault: string = './assets/images/toy.jpg';
  // counter: number = 0;
  // counterFn: number | undefined;

  constructor() {
    // before render
    // No async -- once time
    // console.log('constructor', 'imgValue => ', this.img);
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   // before -- during render
  //   // changes inputs -- many times
  //   console.log('ngOnChanges', 'imgValue => ', this.img);
  //   console.log('changes', changes);
  // }

  // ngAfterViewInit() {
  //   // After render
  //   // handler children
  //   console.log('ngAfterViewInit');
  // }

  // ngOnDestroy() {
  //   // delete
  //   console.log('ngOnDestroy');
  //   // window.clearInterval(this.counterFn);
  // }

  imgError() {
    this.img = this.imageDefault;
  }

  imgLoaded() {
    // console.log('log hijo');
    this.loaded.emit(this.img);
  }
}
